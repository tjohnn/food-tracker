//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Tjohn on 2/15/20.
//  Copyright © 2020 tjohnn. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {

    //MARK: Meal Class Tests
    // Confirm that the Meal initializer returns a Meal object when passed valid parameters.
    func testMealInitializationSucceeds() {
        
        // zero rating meal
        let zeroRatingMeal = Meal.init(name: "Zero", rating: 0, image: nil)
        XCTAssertNotNil(zeroRatingMeal)
        
        // highest positive rating
        let positiveratingMeal = Meal.init(name: "Positive", rating: 5, image: nil)
        XCTAssertNotNil(positiveratingMeal)
    }
    
    // Confirm that the Meal initialier returns nil when passed a negative rating or an empty name.
    func testMealInitializattionFails() {
        
        // Negtive Rating
        let negativeRating = Meal.init(name: "Negative", rating: -1, image: nil)
        XCTAssertNil(negativeRating)
        
        // Rating exceeds maximum
        let largeRatingMeal = Meal.init(name: "Large", rating: 6, image: nil)
        XCTAssertNil(largeRatingMeal)
        
        // Empty String
        let emptyStringMeal = Meal.init(name: "", rating: 0, image: nil)
        XCTAssertNil(emptyStringMeal)
    }

}
