//
//  Meal.swift
//  FoodTracker
//
//  Created by Tjohn on 2/20/20.
//  Copyright © 2020 tjohnn. All rights reserved.
//

import UIKit

class Meal {
    
    // MARK: Properties
    var name: String
    var rating: Int
    var image: UIImage?
    
    //MARK: Initializer
    init?(name: String, rating: Int, image: UIImage?) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // The rating must be between 0 and 5 inclusively
        guard (rating >= 0) && (rating <= 5) else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.rating = rating
        self.image = image
    }
}
